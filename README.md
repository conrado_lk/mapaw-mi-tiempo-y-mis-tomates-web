## Objetivo del proyecto

El objetivo de Mi tiempo y mis tomates es crear una plataforma web que permita al usuario utilizar la técnica de pomodoro, al mismo tiempo, generando reportes basados en estadísticas del usuario provenientes del uso de la plataforma, con el fin de incrementar la productividad personal del mismo. El sistema consiste principalmente en la creación de tareas, en la cual se deberá especificar un nombre y la cantidad de pomodoros estimados que llevará completarla. Luego, el usuario iniciará la tarea y deberá pasar por los períodos de trabajo y descanso de cada pomodoro, emitiendo una alarma personalizable al finalizar cada período, además, estos períodos de tiempo pueden ser configurados por el usuario. El proceso se realizará y repetirá hasta que el usuario complete o abandone la tarea en cuestión.

Se fortalece y se agrega valor a la aplicación web mediante una aplicación de escritorio, que se conecta y relaciona a un usuario registrado mediante autenticación, ésta, se ejecuta en el sistema operativo del usuario. La aplicación de escritorio, durante el tiempo de ejecución de un pomodoro, registra el tiempo en que una aplicación o un sitio web está en foco, para poder analizar luego las razones del incremento o disminución de productividad, información que será explayada y visualizada en los reportes. Estas aplicaciones o sitios web, poseen un nivel de productividad y categoría asociado, que pueden ser configurados por el usuario para obtener un reporte y detalle más preciso.

En definitiva, el sistema quiere acercar al usuario a una experiencia valiosa, sencilla, agradable, y que requiera poco esfuerzo en su uso, además de brindarle la posibilidad de personalizar o configurar ajustes que tengan impacto en los reportes. Consideramos a los reportes como la herramienta más valiosa para el usuario, ya que sirve como una guía para saber donde se está desperdiciando o utilizando incorrectamente el tiempo, para luego poder tomar acción, con el fin de evitarlo o encaminar una mejora.

## Lista de user stories realizadas

* Registrar usuario
* Iniciar sesión
* Ver tareas
* Crear tarea
* Configurar pomodoros
* Configurar cuenta
* Cerrar sesión
* Iniciar tarea
* Cancelar tarea
* Finalizar tarea
* Pausar y reanudar tarea
* Retomar tarea
* Medir el tiempo de uso de aplicaciones
* Medir el tiempo de uso de sitios web
* Visualizar tiempo de trabajo o descanso
* Eliminar tarea
* Enviar email con estadísticas periodicamente

## Modelo UML

![Scheme](https://trello-attachments.s3.amazonaws.com/5b92d92e33649576719dfc8c/5bffcda9e0d86a492d84b0f9/fc579345d4300d4bb0a83ebe8eacd571/Modelo_de_dise%C3%B1o_UML.png)

## Tecnologías y consideraciones 
Se consideraron varias tecnologías al principio del proyecto web, como utilizar VueJS como framework para frontend y PHP para el backend, pero luego decidimos con el equipo de simplificar la arquitectura del sistema lo más posible, con el fin de evitar aprender tecnologías no conocidas para algunos, extendiendo el tiempo de investigación, de desarrollo y finalmente, no aportando considerablemente al objetivo principal de la materia.

Finalmente, se decidió utilizar el framework Symfony 4.1 full-stack de PHP, por la simplicidad y el conocimiento que ya poseíamos de PHP. Tuvimos que aprender e investigar de manera obligatoria, como es usual, ciertos conceptos, técnicas y las herramientas que el framework poseía para agilizar y resolver los problemas de la manera más rápida y correcta posible dentro de los tiempos de entrega que tuvimos. 

Para la aplicación de escritorio se utilizó C# con el framework .NET, ya que el mismo framework tiene un acceso muy documentado y fácil a la API de Windows (sistema operativo en el que pensamos desarrollar la aplicación inicialmente), además de la facilidad con la que se crean interfaces gráficas y se puede conectar con APIs. Aunque la aplicación fue hecha para Windows, también tenemos y evaluamos la posibilidad de portar la aplicación a Linux utilizando Mono por ejemplo, haciendo algunas adaptaciones a la API del sistema operativo, esto es considerado una mejora futura.

En conclusión, el equipo cree que se tomaron buenas decisiones al elegir las tecnologías. Symfony agilizó de manera considerable el desarrollo una vez que nos acomodamos y aprendimos del mismo, permitiéndonos agregar funcionalidades en poco tiempo, como así también reutilizar funcionalidades para hacer otras similares, además de realizar ajustes rápidamente.


## Requerimientos
* PHP 7.1+
* Composer
* Yarn 1.12.1+
* MySQL
* NodeJS 8.11.0+

## Instalación para Debian/Ubuntu/Mint

### PHP 7.2
```
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y php7.2
```

### Composer
```
sudo apt update
sudo apt install curl php-cli php-mbstring git unzip
cd ~
curl -sS https://getcomposer.org/installer -o composer-setup.php
php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer

```

### Extensiones necesarias de PHP 7.2 para Symfony 4.1
```
sudo apt-get install php7.2-xml
sudo apt-get install php7.2-curl
sudo apt-get install php7.2-mbstring
sudo apt-get install php7.2-zip
sudo apt-get install php7.2-mysql
```

### NodeJS
```
sudo apt install curl
curl -sL https://deb.nodesource.com/setup_8.x | sudo bash -
sudo apt install nodejs
```

### YARN
```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update
sudo apt install --no-install-recommends yarn
```

### MySQL
https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04
```
sudo apt update
sudo apt install mysql-server
```

## Instalación para Windows

### PHP 7.2
https://windows.php.net/downloads/releases/php-7.2.11-Win32-VC15-x64.zip
### Composer
https://getcomposer.org/Composer-Setup.exe
### NodeJS
https://nodejs.org/dist/v8.12.0/node-v8.12.0-x86.msi
### YARN
https://yarnpkg.com/latest.msi
### MySQL
https://dev.mysql.com/downloads/installer/

# Instalacion común para Windows/Linux
Configurar usuario y contraseña de MySQL en el archivo ".env"

Posicionarse en la carpeta del proyecto y ejecutar los comandos
```
yarn install
composer install
php bin/console doctrine:database:create (crea la base de datos, DEBE estar configurado el archivo ".env" para que funcione)
php bin/console doctrine:migrations:migrate (crea las tablas de la base de datos en base a los modelos)
php bin/console doctrine:fixtures:load (carga las tablas con datos mock)
```

## Levantar la aplicación
Posicionarse en la carpeta del proyecto y ejecutar el comando
```
yarn dev --watch (esto crea un proceso para que se actualice el "paquete" de la aplicación de archivos JS y CSS mientras se van cambiando, NO se debe cerrar el proceso)
php -S 127.0.0.1:8000 -t public (para "servir" la aplicación)
```

Luego para acceder a la aplicación
http://localhost:8000


## Purga de base de datos de la aplicación

Para purgar la base de datos y restaurarla por defecto:
```
yes | ./renewDB.sh
```